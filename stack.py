# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 19:24:57 2019

@author: ajay_raikar
"""

"""
Stack Data Structure.

A B C D

D <-- top of the stack
C
B
A <-- bottom of the stack
"""

class Stack():
    def __init__(self):
        self.items = []
    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def is_empty(self):
        return self.items == []

    def peek(self):
        if not self.is_empty():
            return self.items[-1]
    def get_stack(self):
        return self.items

s = Stack()
print(s.is_empty()) # True
s.push("A")
s.push("B")
print(s.get_stack()) # ['A', 'B']
s.push("C")
print(s.get_stack()) # ['A', 'B', 'C']
s.pop() # pop top element from stack
print(s.get_stack()) # ['A', 'B']
print(s.is_empty()) # False
print(s.peek()) # return top element from the stack which is 'B'











