# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 19:58:09 2019

@author: ajay_raikar
"""

"""
Use a stack to check whether or not a string has balanced usage of parenthesis.

Example:
    (), (){}, (({{}})) <-- Balanced.
    ((), {{{)}], [][]]] <-- Not Balanced.
    
Balanced Example: {[]}

])}

[
{
Non-Balanced Example: (()

(
Non-Balanced Example: ))
"""
from stack import Stack

def is_paren_balanced(paren_string):
    s = Stack()

    for i in paren_string:
        if i == '}' or i == ']' or i == ')':
            if s.is_empty():
                return "paren are not balanced"
            s.pop()
            print(s.get_stack())
        else:
            s.push(i)
            print(s.get_stack())
    if s.is_empty():
        return "paren are balanced"
    else:
        return "paren are not balanced"

print(is_paren_balanced('))'))

