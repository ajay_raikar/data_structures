# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 23:21:35 2019

@author: d33ps3curity
"""


"""
Use a stack data structure to convert integer values to binary

Example : 242
          n//2     n%2
242 / 2 = 121 ->    0
121 / 2 = 60 ->     1
60 / 2 = 30  ->     0
30 / 2 = 15  ->     0
15 / 2 = 7   ->     1
7 / 2  = 3   ->     1
3 / 2 = 1    ->     1
1 / 2 = 0    ->     1
"""

from stack import Stack

def int_to_binary(num):
    s = Stack()
    # do this operation till num becomes zero
    while num:
    # perfrom module operation on a num and push it to the stack
        # print(num)
        s.push(num%2)
    # perform division operation of a num
        num = num // 2
    # perform check on stack if it is not empty pop the number and print
    while not s.is_empty():
        print(s.pop(), end='')

int_to_binary(242)